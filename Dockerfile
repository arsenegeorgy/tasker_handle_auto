FROM python:3.8-slim-buster
MAINTAINER arsene
WORKDIR /code
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git
RUN git clone https://gitlab.com/arsenegeorgy/tasker_handler.git .
# RUN ls
# RUN rm tasker_handler/Dockerfile
# COPY tasker_handler/requirements.txt .
# COPY requirements.txt .
RUN pip install -r requirements.txt
# COPY tasker_handler/appli/ .
CMD [ "python", "./appli/lancement.py" ]
